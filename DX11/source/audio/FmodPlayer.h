///////////////////////////////////////////////////////////////////////////
//	File Name	:	"FmodPlayer.h"
//	
//	Author Name	:	JC Ricks
//	
//	Purpose		:	Encapsulate FMOD functions
///////////////////////////////////////////////////////////////////////////
#ifndef _FMODPLAYER_H
#define _FMODPLAYER_H

////////////////////////////////////////
//				INCLUDES
////////////////////////////////////////
#include <fmod.hpp>
#include "../Globals.h"
////////////////////////////////////////
//		   FORWARD DECLARATIONS
////////////////////////////////////////

////////////////////////////////////////
//				MISC
////////////////////////////////////////


class FmodPlayer
{
public:
	/********** Construct / Deconstruct / OP Overloads ************/
	FmodPlayer();
	~FmodPlayer();
	/********** Public Utility Functions ************/
	bool Initialize();
	bool Update(float deltaTime);
	void Shutdown();

	/********** Public Accessors ************/

	/********** Public Mutators  ************/

private:
	/********** Private Members ************/

	// FMOD System
	FMOD::System* mSystem;

	/********** Private Accessors ************/

	/********** Private Mutators ************/

	/********** Private Utility Functions ************/

};
#endif