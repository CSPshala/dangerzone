///////////////////////////////////////////////////////////////////////////
//	File Name	:	"AudioPlayer.h"
//	
//	Author Name	:	JC Ricks
//	
//	Purpose		:	Encapsulates audio playing
///////////////////////////////////////////////////////////////////////////
#ifndef _AUDIOPLAYER_H
#define _AUDIOPLAYER_H

////////////////////////////////////////
//				INCLUDES
////////////////////////////////////////

////////////////////////////////////////
//		   FORWARD DECLARATIONS
////////////////////////////////////////
class FmodPlayer;

////////////////////////////////////////
//				MISC
////////////////////////////////////////


class AudioPlayer
{
public:
	/********** Public Utility Functions ************/
	static AudioPlayer* GetInstance();
	static void DeleteInstance();	

	bool Initialize();
	bool Update(float deltaTime);
	void Shutdown();
	/********** Public Accessors ************/

	/********** Public Mutators  ************/

private:
	/********** Construct / Deconstruct / OP Overloads ************/
	AudioPlayer();
	AudioPlayer(const AudioPlayer&);
	~AudioPlayer();
	AudioPlayer& operator=(const AudioPlayer&);
	/********** Private Members ************/
	static AudioPlayer* m_instance;
	
	// Fmod wrapper
	FmodPlayer* mPlayer;
	/********** Private Accessors ************/

	/********** Private Mutators ************/

	/********** Private Utility Functions ************/

};
#endif